import { createStore } from "vuex";

export default createStore({
  state: {
    personalInfo: {
      firstName: "hossein",
      lastName: "taromi",
      avatar: "https://cdn.vuetifyjs.com/images/john.png",
      email: "hossein@gmail.com",
    },
    chatList: [
      {
        id: 1,
        prependAvatar: "https://cdn.vuetifyjs.com/images/lists/1.jpg",
        title: "Brunch this weekend?",
        subtitle: `Ali Connors I'll be in your neighborhood doing errands this weekend. Do you want to hang out?`,
      },
      {
        id: 2,
        prependAvatar: "https://cdn.vuetifyjs.com/images/lists/2.jpg",
        title: "Summer BBQ",
        subtitle: `Ali Connors I'll be in your neighborhood doing errands this weekend. Do you want to hang out?`,
      },
      {
        id: 3,
        prependAvatar: "https://cdn.vuetifyjs.com/images/lists/3.jpg",
        title: "Oui oui",
        subtitle: `Ali Connors I'll be in your neighborhood doing errands this weekend. Do you want to hang out?`,
      },
      {
        id: 4,
        prependAvatar: "https://cdn.vuetifyjs.com/images/lists/4.jpg",
        title: "Birthday gift",
        subtitle: `Ali Connors I'll be in your neighborhood doing errands this weekend. Do you want to hang out?`,
      },
      {
        id: 5,
        prependAvatar: "https://cdn.vuetifyjs.com/images/lists/5.jpg",
        title: "Recipe to try",
        subtitle: `Ali Connors I'll be in your neighborhood doing errands this weekend. Do you want to hang out?`,
      },
    ],
  },
  mutations: {
    UPDATE_PROFILE(state, payload) {
      state.personalInfo = payload;
    },
  },
  actions: {
    updateProfile(context, payload) {
      const newProfile = context.state.personalInfo;
      console.log(newProfile);
      newProfile.firstName = payload.firstName;
      newProfile.lastName = payload.lastName;
      newProfile.email = payload.email;

      context.commit("UPDATE_PROFILE", newProfile);
    },
  },
  getters: {
    fullName: function (state) {
      return `${state.personalInfo.firstName} ${state.personalInfo.lastName}`;
    },
  },
});
