export default {
  methods: {
    whatTimeIsIt() {
      const today = new Date();
      return today.getHours() + ":" + today.getMinutes();
    },
  },
};
